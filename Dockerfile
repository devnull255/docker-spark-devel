FROM ubuntu:latest
RUN apt-get update && apt-get install -y ipython3 vim openjdk-8-jdk wget && ln -s /usr/bin/ipython3 /usr/bin/python
RUN wget https://www-eu.apache.org/dist/spark/spark-2.4.3/spark-2.4.3-bin-hadoop2.7.tgz && \
   cd /usr/local && \
   tar xvzf /spark-2.4.3-bin-hadoop2.7.tgz && \
   ln -s spark-2.4.3-bin-hadoop2.7 spark && rm /spark-2.4.3-bin-hadoop2.7.tgz
ADD 10-spark.sh /etc/profile.d

